<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\AccountRepository;
use App\Repository\OperationRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Operation;
use App\Entity\Account;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/api/user/account", name="account")
 */
class AccountController extends Controller

{

    private $serializer;

    /**
     * @var SerializerInterface $serial
     */
    private $serial;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'banque', 'name', 'solde']];

    public function __construct(SerializerInterface $serializer)
    {
        $this->serial = $serializer;
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/", methods={"POST"})
     */

    public function addOneAccount(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $content = $request->getContent();

        $user = $this->getUser();

        $account = $this->serializer->deserialize($content, Account::class, 'json');

        $account->setUser($user);
        var_dump($user);
        $manager->persist($account);
        $manager->flush();

        $data = $this->serializer->normalize($account, null, [self::NORMALIZER_FORMAT]);

        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }


    /**
     * @Route("/", methods={"GET"})
     */
    public function findAll(AccountRepository $repo)
    {

        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }

    /**
     * @Route("/{id}", methods={"GET"})
     */
    public function findOne(AccountRepository $repo, int $id)
    {

        $list = $repo->find($id);

        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }



    /**
     * @Route("/{account}/operation/", name="new_operation", methods={"POST"})
     * 
     */
    public function addOperation(Account $account, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $operation = $this->serial->deserialize($content, Operation::class, "json");


        $account->addOperation($operation);
        $account->computeBalance($operation);

        $manager->persist($account);
        $manager->flush();

        $data = $this->serializer->normalize($operation, null, ['attributes' => ['id', 'montant', 'account']]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
    }

    /**
     * @Route("/{idAccount}/operation/{id}", name="remove_operation", methods={"DELETE"})
     */
    public function removeOperation(int $idAccount, AccountRepository $repo, OperationRepository $opeRepo, int $id)
    {
        $manager = $this->getDoctrine()->getManager();


        $operation = $opeRepo->find($id);

        //$repo = $this->getDoctrine()->getManager()->getRepository("AccountRepository");


        $account = $repo->find($idAccount);
        $account->computeBalance($operation, true);
        $manager->remove($operation);
        $manager->persist($account);
        $manager->flush();

        return new Response(204);
    }
}
