<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Operation;
use App\Repository\OperationRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Types\JsonArrayType;
use Symfony\Component\HttpFoundation\JsonResponse;
use phpDocumentor\Reflection\Types\Integer;
use App\Repository\AccountRepository;

class OperationController extends Controller
{

    private $serializer;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'montant','account']];

    public function __construct()
    {
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/api/operation", methods={"GET"})
     */
    public function findAllOperation(OperationRepository $repo)
    {

        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }

    /**
     * @Route("/api/user/account/{id}/operation", methods={"GET"})
     */
    public function findAllByAccountID(OperationRepository $repo, $id)
    {

        $list = $repo->findBy(
            ["account" => $id]
        );

        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }


    //TO READ LATER 
    // /**
    //  * @Route("/account/{id}", methods={"POST"})
    //  */
    // public function add(int $id,AccountRepository $accountRepo,Request $request)
    // {

    //     $manager = $this->getDoctrine()->getManager();


    //     if ($request->isSecure()) {
    //         $content = $request->getContent();

    //         $account = $accountRepo->find($id);

    //         $operation = new Operation();
    //         $operation = $this->serializer->deserialize($content, Operation::class, "json");
    //         $operation->setAccount($account);



    //         $data = $this->serializer->normalize($operation, null, [self::NORMALIZER_FORMAT]);

    //         $manager->persist($operation);
    //         $manager->flush();

    //         $response = new Response($this->serializer->serialize($operation, "json"));


    //         return $response;
    //     } else {
    //         $response = new Response();

    //         return $response->setStatusCode("500", "Check the type of the inputs there is an error of type");
    //     }

    // }
}
