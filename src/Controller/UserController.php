<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\AccountRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;



class UserController extends Controller

{
    private $serializer;

    /**
     * @var SerializerInterface $serial
     */
    private $serial;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'banque', 'name', 'solde']];

    public function __construct(SerializerInterface $serializer)
    {
        $this->serial = $serializer;
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }


    /**
     * @Route("/api/user", methods={"POST"})
     */
    public function addUser(Request $req, AccountRepository $user,UserPasswordEncoderInterface $encoder)
    {


        $manager = $this->getDoctrine()->getManager();

        $message = $this->serializer->deserialize(
            $req->getContent(),
            User::class,
            "json"
        );
        $message->setPassword($encoder->encodePassword($message, $message->getPassword()));
        $manager->persist($message);
        $manager->flush();

        $json = $this->serializer->serialize($message, "json");

  
        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/api/user", methods={"GET"})
     */

     public function findAllUserAccount(AccountRepository $repo)
     {
         $user = $this->getUser();
         $accounts = $repo->findBy(
             ['user' => $user]
         );

         $data = $this->serializer->normalize($accounts, null, [self::NORMALIZER_FORMAT]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"),201);
         
     }
}



