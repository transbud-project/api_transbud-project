<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/categories")
 */
class CategoryController extends AbstractController {

  /**
   * @Route("/", methods={"GET"})
   */
  public function getAllCategories(CategoryRepository $categoryRepository, SerializerInterface $serializer) {
    $categories = $categoryRepository->findAll();
    return new JsonResponse($serializer->serialize($categories, 'json'), 200, [], true);
  }
}
