<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;
use App\Entity\Account;
use App\Entity\Category;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class OperationFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {



        $categories = [];
        for ($i = 0; $i < 12; $i++) {
            $category = new Category();
            $categoryList = ["Personal","Giving", "Food", "Shelter", "Utilities", "Clothing", "Transportation", "Medical", "Insurance", "Supplies", "Saving", "Fun"];
            $category->setLabel($categoryList[$i]);
            $manager->persist($category);
            $categories[] = $category;
        }


        $user = new User();
        $user->setName('User ' . $i);
        $user->setSurname('grou');
        $user->setEmail("user{$i}@mail.fr");

        $password = 'foobar';
        $user->setPassword($this->encoder->encodePassword($user, $password));

        $manager->persist($user);


        $tab = [123, 456, -12345, 12, 61, -123456, 5];
        $account = new Account();
        $manager->persist($account);

        foreach ($tab as $key => $value) {

            $date = new \DateTime();

            $operation = new Operation();

            $operation->setAmount($value);

            $account->setBank("Piggy Bank");
            $account->setName("Personal Account");
            $account->setBalance(16);
            $account->setUser($user);

            $operation->setAccount($account);
            $operation->setCategory($categories[array_rand($categories, 1)]);
            $operation->setDate($date);

            $manager->persist($operation);


        }
        $manager->flush();

    }
}
