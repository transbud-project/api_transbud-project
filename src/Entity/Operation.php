<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="operations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function getId()
    {
        return $this->id;
    }

    public function getamount(): ?int
    {
        return $this->amount;
    }

    public function setamount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        if ($date instanceof \DateTime) {
            $this->date = $date;
        } elseif(is_string($date)) {
            $this->date = \DateTime::createFromFormat("d/m/Y", $date);
		if(!$this->date) {
                $this->date = new \DateTime($date);
            }

        } elseif(is_int($date)) {
            $this->date = new \DateTime();
            $this->date->setTimestamp($date / 1000);
        }
        if(!$this->date instanceof \DateTime) {
            throw new \InvalidArgumentException("Date must be d/m/Y or timestamp or DateTime");
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->date = new \DateTime();
    }

}
