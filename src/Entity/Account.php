<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $bank;

    /**
     * @ORM\Column(type="integer")
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="account", orphanRemoval=true, cascade={"persist"})
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

   
    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getBank() : ? string
    {
        return $this->bank;
    }

    public function setBank(string $bank) : self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getBalance() : ? int
    {
        return $this->balance;
    }

    public function setBalance(int $balance) : self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations() : Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation) : self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setAccount($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation) : self
    {
        if ($this->operations->contains($operation)) {
            $this->operations->removeElement($operation);
            // set the owning side to null (unless already changed)
            if ($operation->getAccount() === $this) {
                $operation->setAccount(null);
            }
        }

        return $this;
    }

    public function computeBalance($operation, $remove = false)
    {
        $balance = $this->balance;

        if (!$remove) {
            $this->balance += $operation->getAmount();
        }
        else
        {
            $this->balance -= $operation->getAmount();
        }

        return $this;


    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
